'use strict';

document.addEventListener('DOMContentLoaded', (e) => {
    const btn = document.querySelector('button');
    const table = document.createElement('table');

//createTable function that takes in num of rows/columns and 2 colours
/** This is a function that takes in 4 parameters: number of rows, columns, colour1 and colour2.
 * This is create a table based on the user input
 */
function createTable(row, col, colour1, colour2){

    /**when creating a new table, it will overwrite the current one */
    table.textContent = undefined;

    /**saving input values into variable s*/
    let rowInput = document.querySelector('#rows').value;
    let colInput = document.querySelector('#columns').value;
    let colour1Input = document.querySelector('#colour1').value;
    let colour2Input = document.querySelector('#colour2').value;

    /**saving location to where the table will be */
    const location = document.querySelector('#tableHere');

    /**outer loop will create a row and inner loop with create columns
    /*using user input of row and column as condition */
    for(let i = 0; i < rowInput; i++){
        /** for every row, create a new tr */
        const row = document.createElement('tr');
        table.appendChild(row);

        for(let j = 0; j < colInput; j++){
            /**for each row, create td */
            const col = document.createElement('td');
            col.innerText = i + ' ' + j;
            table.appendChild(col);

            /**condition on which color each td will take */
            if( (j+i) % 2 === 0){
                col.style.backgroundColor = colour1Input;
            }
            else{
                col.style.backgroundColor = colour2Input;
            }
        }
    }
    /**inserts table inside specified section */
    location.appendChild(table);
    /** Will prevent the default action of the form so we can use the user inputs */
    e.preventDefault();
}
    btn.addEventListener('click', createTable);
});